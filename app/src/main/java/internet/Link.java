package internet;

import internet.models.Resp;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface Link {
    @GET("/1/wallpapers")
    Call<Resp> listImages(@Query("page") int page);
}
