package internet.models;

import java.io.Serializable;
import java.util.List;

public class Response implements Serializable {
    @com.google.gson.annotations.SerializedName("id")
    private int id;
    @com.google.gson.annotations.SerializedName("bytes")
    private int bytes;
    @com.google.gson.annotations.SerializedName("created_at")
    private String createdAt;
    @com.google.gson.annotations.SerializedName("image")
    private Image image;
    @com.google.gson.annotations.SerializedName("height")
    private int height;
    @com.google.gson.annotations.SerializedName("width")
    private int width;
    @com.google.gson.annotations.SerializedName("review_state")
    private String reviewState;
    @com.google.gson.annotations.SerializedName("uploader")
    private String uploader;
    @com.google.gson.annotations.SerializedName("user_count")
    private int userCount;
    @com.google.gson.annotations.SerializedName("likes_count")
    private int likesCount;
    @com.google.gson.annotations.SerializedName("palette")
    private List<String> palette;
    @com.google.gson.annotations.SerializedName("url")
    private String url;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBytes() {
        return bytes;
    }

    public void setBytes(int bytes) {
        this.bytes = bytes;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public String getReviewState() {
        return reviewState;
    }

    public void setReviewState(String reviewState) {
        this.reviewState = reviewState;
    }

    public String getUploader() {
        return uploader;
    }

    public void setUploader(String uploader) {
        this.uploader = uploader;
    }

    public int getUserCount() {
        return userCount;
    }

    public void setUserCount(int userCount) {
        this.userCount = userCount;
    }

    public int getLikesCount() {
        return likesCount;
    }

    public void setLikesCount(int likesCount) {
        this.likesCount = likesCount;
    }

    public List<String> getPalette() {
        return palette;
    }

    public void setPalette(List<String> palette) {
        this.palette = palette;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
