package internet.models;

import java.io.Serializable;

public class Preview implements Serializable {
    @com.google.gson.annotations.SerializedName("url")
    private String url;
    @com.google.gson.annotations.SerializedName("width")
    private int width;
    @com.google.gson.annotations.SerializedName("height")
    private int height;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }
}
