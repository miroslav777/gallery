package internet.models;

import java.io.Serializable;

public class Pagination implements Serializable {
    @com.google.gson.annotations.SerializedName("current")
    private int current;
    @com.google.gson.annotations.SerializedName("previous")
    private String previous;
    @com.google.gson.annotations.SerializedName("next")
    private int next;
    @com.google.gson.annotations.SerializedName("per_page")
    private int perPage;
    @com.google.gson.annotations.SerializedName("pages")
    private int pages;
    @com.google.gson.annotations.SerializedName("count")
    private int count;

    public int getCurrent() {
        return current;
    }

    public void setCurrent(int current) {
        this.current = current;
    }

    public String getPrevious() {
        return previous;
    }

    public void setPrevious(String previous) {
        this.previous = previous;
    }

    public int getNext() {
        return next;
    }

    public void setNext(int next) {
        this.next = next;
    }

    public int getPerPage() {
        return perPage;
    }

    public void setPerPage(int perPage) {
        this.perPage = perPage;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
