package internet.models;

import java.io.Serializable;
import java.util.List;


public class Resp implements Serializable{

    @com.google.gson.annotations.SerializedName("response")
    private List<Response> response;
    @com.google.gson.annotations.SerializedName("count")
    private int count;
    @com.google.gson.annotations.SerializedName("pagination")
    private Pagination pagination;

    public List<Response> getResponse() {
        return response;
    }

    public void setResponse(List<Response> response) {
        this.response = response;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public Pagination getPagination() {
        return pagination;
    }

    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }
}
