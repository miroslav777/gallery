package internet;

import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import internet.models.Resp;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitUtil extends AsyncTask<Integer,Void,Resp> {

    private  Gson gson = new GsonBuilder().create();
    private  String baseUrl = "https://api.desktoppr.co";
    private  Retrofit retrofit = new Retrofit.Builder()
           .addConverterFactory(GsonConverterFactory.create(gson))
           .baseUrl(baseUrl)
           .build();

    private Link link = retrofit.create(Link.class);

    @Override
    protected Resp doInBackground(Integer... integers) {
        Call<Resp> call = link.listImages(integers[0]);
        Response<Resp> response = null;
        try {
            response = call.execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response.body();
    }
}
