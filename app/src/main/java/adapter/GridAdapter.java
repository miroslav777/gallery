package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import gallery.com.example.R;
import internet.models.Response;


public class GridAdapter extends BaseAdapter {

    private Context context;
    private List<Response> list;

    public GridAdapter(Context applicationContext, List<Response> list) {
        this.context = applicationContext;
        this.list = list;
    }

    static class ViewHolder{

        @BindView(R.id.imageViewItem)
        ImageView imageView;

        @BindView(R.id.textViewInfo)
        TextView textView;

        public ViewHolder(View view){
            ButterKnife.bind(this, view);
        }

    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return list.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.item, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        }
        else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        Response rep = list.get(position);
        Picasso.with(context)
                .load(rep.getImage().getUrl())
                .resize(150,100)
                .centerCrop()
                .into(viewHolder.imageView);
        viewHolder.textView.setText("likes: " + rep.getLikesCount());
        return convertView;
    }
}
