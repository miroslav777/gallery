package adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.squareup.picasso.Picasso;

import gallery.com.example.R;
import internet.models.Resp;


public class ViewPagerAdapter extends PagerAdapter {

    private Resp resp;
    private Context context;
    LayoutInflater inflater;
    public static String RESP_OBJ = "RESP_OBJ";


    public ViewPagerAdapter(Context context, Resp resp){
        this.context = context;
        this.resp = resp;
    }

    @Override
    public int getCount() {
        return resp.getResponse().size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.item, container,
                false);

        ImageView imageView = (ImageView) itemView.findViewById(R.id.imageViewItem);

        String URL = resp.getResponse().get(position).getImage().getPreview().getUrl();

        Picasso.with(context)
                .load(URL)
                .fit()
                .into(imageView);
        ((ViewPager) container).addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((LinearLayout) object);
    }
}
