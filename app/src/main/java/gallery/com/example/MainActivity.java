package gallery.com.example;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import java.util.concurrent.ExecutionException;

import adapter.GridAdapter;
import adapter.ViewPagerAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import internet.RetrofitUtil;
import internet.models.Resp;


public class MainActivity extends Activity {


    @BindView(R.id.gridView)
    GridView gridView;

    private Resp resp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @Override
    protected void onResume() {
        super.onResume();



        if (!isOnline()){
            Toast.makeText(getApplicationContext(), "No internet connection", Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    finish();
                }
            }, Toast.LENGTH_LONG);
        }
        else {
            RetrofitUtil ret = new RetrofitUtil();
            ret.execute(1);

            try {
                resp = ret.get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }

            GridAdapter gridAdapter = new GridAdapter(this.getApplicationContext(),resp.getResponse());
            gridView.setAdapter(gridAdapter);
            gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(MainActivity.this, ViewPagerActivity.class);
                    intent.putExtra(ViewPagerAdapter.RESP_OBJ,resp);
                    startActivity(intent);
                }
            });
        }
    }

    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}
