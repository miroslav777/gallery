package gallery.com.example;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;

import adapter.ViewPagerAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import internet.models.Resp;

public class ViewPagerActivity extends Activity {

    @BindView(R.id.view_pager)
    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_pager);
        ButterKnife.bind(this);
        Intent intent = getIntent();
        Resp  resp = (Resp) intent.getSerializableExtra(ViewPagerAdapter.RESP_OBJ);
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(this.getApplicationContext(),resp);
        viewPager.setAdapter(viewPagerAdapter);
    }
}
